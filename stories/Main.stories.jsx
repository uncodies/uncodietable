
import Main from "../src/components/Main";
import React from "react";

const cellComponent = ({ data }) => <div style={{ border: '1px solid green'}} >{data}</div>;
const rowComponent = ({ children }) => <div style={{ border: '1px solid red', display: 'flex', justifyContent: 'space-around'}}>{children}</div>;
const HeaderComponent = ({ children }) => <div style={{ border: '1px solid blue', display: 'flex', justifyContent: 'space-around' }}>{children}</div>;
const cellHeaderComponent = ({ data }) => <div style={{ border: '1px solid yellow', }}>{data}</div>;

const data = [
  {
    "branch": {
      "name": "Plaza Ana"
    },
    "create_date": "2024-04-04",
    "departament": {
      "name": "Cocina"
    },
    "note": null
  },
  {
    "branch": {
      "name": "Plaza hidalgo"
    },
    "create_date": null,
    "departament": {
      "name": "Garden"
    },
    "note": "fdafadfaffa"
  },
  {
    "branch": {
      "name": "Plaza Ana"
    },
    "create_date": null,
    "departament": {
      "name": "Cocina"
    },
    "note": null
  },
  {
    "branch": {
      "name": "Plaza Ana"
    },
    "create_date": null,
    "departament": {
      "name": "Cocina"
    },
    "note": null
  },
  {
    "branch": {
      "name": "Plaza Ana"
    },
    "create_date": null,
    "departament": {
      "name": "Cocina"
    },
    "note": null
  }
];
export default {
  title: "Example/Main",
  component: Main,
}

const Template = (args) => {
  const childCompRef = React.useRef();
  const obtainHTML = value => console.log(value);
  React.useEffect(() => {
    setTimeout(() => {
      childCompRef.current?.handleSave();
    }, 5000);
  }, []);
  return <Main {...args} />;
}

export const Primary = Template.bind({});
Primary.args = {
  divId: 'main',
  PrimaryField: '_id',
  data: data,
  style: { border: '1px solid black', padding: '10px' },
  collection: 'Users',
  cssClass: 'main',
  fields: null,
  headerComponent: HeaderComponent,
  
  onCreate: () => console.log('Create'),
  onEdit: () => console.log('Edit'),
  onDeleteItem: () => console.log('Delete'),
  onDetall: () => console.log('Detail'),
  onClickRow: (e) => console.log('click row', e),
  PrimarButton: () => <button style={{ border: '1px solid orange'}}>Primary</button>
}