import React from 'react';
const DefaultHeader= ({ children }) => <div>{children}</div>
const DefaultCell= ({ data }) => <div>{data}</div>
const Header = ({HeaderComponent, fields, CellComponent})=>{
    const [ _fields, setFields ] = React.useState(fields);
    React.useEffect(()=>{
        setFields(fields);
    },[fields]);
    
    const HeaderComp = HeaderComponent || DefaultHeader;
    const HeaderCell = CellComponent || DefaultCell;
    return (
        <HeaderComp>
            {
                (_fields || [] ).map((field, index) => {
                    return <HeaderCell key={`cell-header-${index}`} data={field.title || field.toString()} />
                })
            }
        </HeaderComp>
    )
    
   
}

export default Header;