import React from 'react';
const DefaultRow = (props) => <div { ...props }>{props.children}</div>
const getValue = (data) => {
    const type = typeof data;
    switch(type){
        case 'object':
            return !Array.isArray(data) ? data?.name || data?.id || '' : JSON.stringify(data);
        default:
            return data || '';
    }
}
const FieldCellComponent = ({ data, field, CellComponent }) => {
    const value = getValue(data);
    if(field && field.cellComponent)
        return(<field.cellComponent data={value}  />)
    if(CellComponent)
        return (<CellComponent data={value} />)
    return(<div>{value}</div>)
}
const Content = ({RowComponent, CellComponent, fields, data, PrimaryField, onClickRow})=>{
    const onClickItem = (item)=>{
        if(!PrimaryField)
            return;
        if(!(typeof onClickRow === 'function'))
            return;
        if(!item[PrimaryField])
            return;
        onClickRow({ value: item[PrimaryField] });
    }
    const getRow = (Component)=>(data || []).map((item, index) => 
                    <Component key={`row-${index}`} onClick={()=>onClickItem(item)}>
                        {
                            (fields ? fields.map(item=>item.name) : Object.keys(item)).map((field, indexField)=><FieldCellComponent field={(fields || [])[indexField]} key={`${index}:${indexField}`} data={item[field]} CellComponent={CellComponent} />)
                        }
                    </Component>
                )
                
    const Row = RowComponent ||   DefaultRow 
    return(
        <div>
            {
                getRow(Row)
            }
        </div>
    )
}

export default Content;