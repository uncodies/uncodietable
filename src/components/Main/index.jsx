import React from 'react';
import Header from '../Header';
import Content from '../Content';
const getfieldsFromData = (data)=>{
    if(!data || !data.length) return [];
    return Object.keys(data[0]);
}
const Main = ({ divId, PrimaryField, data, fields, style, collection, cssClass, headerComponent, rowComponent, cellComponent, cellHeaderComponent, onCreate, onEdit, onDeleteItem, onDetall, onClickRow })=>{
  
    const [ _data, setData ] = React.useState(data);
    React.useEffect(()=>{
        setData(data);
    },[data]);
    const onDetailHandle = (id, index) => {
        if(typeof onDetall === 'function') onDetall(id);
    }

    const onEditHandle = (id, index) => {
        if(typeof onEdit === 'function') onEdit(id);
    }

    const onDeleteHandle = (id, index) => {
        if(typeof onDeleteItem === 'function') onDeleteItem(id);
    }
    const _filds = fields ? fields : getfieldsFromData(data).map(item=>({ title: item, name: item }));
    return(
        <div id={divId} className={cssClass} style={style}>
            
            <Header HeaderComponent={headerComponent} fields={_filds} CellComponent={cellHeaderComponent || cellComponent} />
            <Content RowComponent={rowComponent} CellComponent={cellComponent} data={data} fields={_filds} PrimaryField={PrimaryField} onDetall={onDetailHandle} onEdit={onEditHandle} onDelete={onDeleteHandle} onClickRow={onClickRow} />
        </div>
    )
}

export default Main;