export default Content;
declare function Content({ RowComponent, CellComponent, fields, data, PrimaryField, onClickRow }: {
    RowComponent: any;
    CellComponent: any;
    fields: any;
    data: any;
    PrimaryField: any;
    onClickRow: any;
}): React.JSX.Element;
import React from "react";
