'use strict';

var React = require('react');

function _interopDefaultLegacy (e) { return e && typeof e === 'object' && 'default' in e ? e : { 'default': e }; }

var React__default = /*#__PURE__*/_interopDefaultLegacy(React);

function _iterableToArrayLimit(r, l) {
  var t = null == r ? null : "undefined" != typeof Symbol && r[Symbol.iterator] || r["@@iterator"];
  if (null != t) {
    var e,
      n,
      i,
      u,
      a = [],
      f = !0,
      o = !1;
    try {
      if (i = (t = t.call(r)).next, 0 === l) {
        if (Object(t) !== t) return;
        f = !1;
      } else for (; !(f = (e = i.call(t)).done) && (a.push(e.value), a.length !== l); f = !0);
    } catch (r) {
      o = !0, n = r;
    } finally {
      try {
        if (!f && null != t.return && (u = t.return(), Object(u) !== u)) return;
      } finally {
        if (o) throw n;
      }
    }
    return a;
  }
}
function _typeof(o) {
  "@babel/helpers - typeof";

  return _typeof = "function" == typeof Symbol && "symbol" == typeof Symbol.iterator ? function (o) {
    return typeof o;
  } : function (o) {
    return o && "function" == typeof Symbol && o.constructor === Symbol && o !== Symbol.prototype ? "symbol" : typeof o;
  }, _typeof(o);
}
function _slicedToArray(arr, i) {
  return _arrayWithHoles(arr) || _iterableToArrayLimit(arr, i) || _unsupportedIterableToArray(arr, i) || _nonIterableRest();
}
function _arrayWithHoles(arr) {
  if (Array.isArray(arr)) return arr;
}
function _unsupportedIterableToArray(o, minLen) {
  if (!o) return;
  if (typeof o === "string") return _arrayLikeToArray(o, minLen);
  var n = Object.prototype.toString.call(o).slice(8, -1);
  if (n === "Object" && o.constructor) n = o.constructor.name;
  if (n === "Map" || n === "Set") return Array.from(o);
  if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen);
}
function _arrayLikeToArray(arr, len) {
  if (len == null || len > arr.length) len = arr.length;
  for (var i = 0, arr2 = new Array(len); i < len; i++) arr2[i] = arr[i];
  return arr2;
}
function _nonIterableRest() {
  throw new TypeError("Invalid attempt to destructure non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method.");
}

var DefaultHeader = function DefaultHeader(_ref) {
  var children = _ref.children;
  return /*#__PURE__*/React__default["default"].createElement("div", null, children);
};
var DefaultCell = function DefaultCell(_ref2) {
  var data = _ref2.data;
  return /*#__PURE__*/React__default["default"].createElement("div", null, data);
};
var Header = function Header(_ref3) {
  var HeaderComponent = _ref3.HeaderComponent,
    fields = _ref3.fields,
    CellComponent = _ref3.CellComponent;
  var _React$useState = React__default["default"].useState(fields),
    _React$useState2 = _slicedToArray(_React$useState, 2),
    _fields = _React$useState2[0],
    setFields = _React$useState2[1];
  React__default["default"].useEffect(function () {
    setFields(fields);
  }, [fields]);
  var HeaderComp = HeaderComponent || DefaultHeader;
  var HeaderCell = CellComponent || DefaultCell;
  return /*#__PURE__*/React__default["default"].createElement(HeaderComp, null, (_fields || []).map(function (field, index) {
    return /*#__PURE__*/React__default["default"].createElement(HeaderCell, {
      key: "cell-header-".concat(index),
      data: field.title || field.toString()
    });
  }));
};

var DefaultRow = function DefaultRow(props) {
  return /*#__PURE__*/React__default["default"].createElement("div", props, props.children);
};
var getValue = function getValue(data) {
  var type = _typeof(data);
  switch (type) {
    case 'object':
      return !Array.isArray(data) ? (data === null || data === void 0 ? void 0 : data.name) || (data === null || data === void 0 ? void 0 : data.id) || '' : JSON.stringify(data);
    default:
      return data || '';
  }
};
var FieldCellComponent = function FieldCellComponent(_ref) {
  var data = _ref.data,
    field = _ref.field,
    CellComponent = _ref.CellComponent;
  var value = getValue(data);
  if (field && field.cellComponent) return /*#__PURE__*/React__default["default"].createElement(field.cellComponent, {
    data: value
  });
  if (CellComponent) return /*#__PURE__*/React__default["default"].createElement(CellComponent, {
    data: value
  });
  return /*#__PURE__*/React__default["default"].createElement("div", null, value);
};
var Content = function Content(_ref2) {
  var RowComponent = _ref2.RowComponent,
    CellComponent = _ref2.CellComponent,
    fields = _ref2.fields,
    data = _ref2.data,
    PrimaryField = _ref2.PrimaryField,
    onClickRow = _ref2.onClickRow;
  var onClickItem = function onClickItem(item) {
    if (!PrimaryField) return;
    if (!(typeof onClickRow === 'function')) return;
    if (!item[PrimaryField]) return;
    onClickRow({
      value: item[PrimaryField]
    });
  };
  var getRow = function getRow(Component) {
    return (data || []).map(function (item, index) {
      return /*#__PURE__*/React__default["default"].createElement(Component, {
        key: "row-".concat(index),
        onClick: function onClick() {
          return onClickItem(item);
        }
      }, (fields ? fields.map(function (item) {
        return item.name;
      }) : Object.keys(item)).map(function (field, indexField) {
        return /*#__PURE__*/React__default["default"].createElement(FieldCellComponent, {
          field: (fields || [])[indexField],
          key: "".concat(index, ":").concat(indexField),
          data: item[field],
          CellComponent: CellComponent
        });
      }));
    });
  };
  var Row = RowComponent || DefaultRow;
  return /*#__PURE__*/React__default["default"].createElement("div", null, getRow(Row));
};

var getfieldsFromData = function getfieldsFromData(data) {
  if (!data || !data.length) return [];
  return Object.keys(data[0]);
};
var Main = function Main(_ref) {
  var divId = _ref.divId,
    PrimaryField = _ref.PrimaryField,
    data = _ref.data,
    fields = _ref.fields,
    style = _ref.style;
    _ref.collection;
    var cssClass = _ref.cssClass,
    headerComponent = _ref.headerComponent,
    rowComponent = _ref.rowComponent,
    cellComponent = _ref.cellComponent,
    cellHeaderComponent = _ref.cellHeaderComponent;
    _ref.onCreate;
    var onEdit = _ref.onEdit,
    onDeleteItem = _ref.onDeleteItem,
    onDetall = _ref.onDetall,
    onClickRow = _ref.onClickRow;
  var _React$useState = React__default["default"].useState(data),
    _React$useState2 = _slicedToArray(_React$useState, 2);
    _React$useState2[0];
    var setData = _React$useState2[1];
  React__default["default"].useEffect(function () {
    setData(data);
  }, [data]);
  var onDetailHandle = function onDetailHandle(id, index) {
    if (typeof onDetall === 'function') onDetall(id);
  };
  var onEditHandle = function onEditHandle(id, index) {
    if (typeof onEdit === 'function') onEdit(id);
  };
  var onDeleteHandle = function onDeleteHandle(id, index) {
    if (typeof onDeleteItem === 'function') onDeleteItem(id);
  };
  var _filds = fields ? fields : getfieldsFromData(data).map(function (item) {
    return {
      title: item,
      name: item
    };
  });
  return /*#__PURE__*/React__default["default"].createElement("div", {
    id: divId,
    className: cssClass,
    style: style
  }, /*#__PURE__*/React__default["default"].createElement(Header, {
    HeaderComponent: headerComponent,
    fields: _filds,
    CellComponent: cellHeaderComponent || cellComponent
  }), /*#__PURE__*/React__default["default"].createElement(Content, {
    RowComponent: rowComponent,
    CellComponent: cellComponent,
    data: data,
    fields: _filds,
    PrimaryField: PrimaryField,
    onDetall: onDetailHandle,
    onEdit: onEditHandle,
    onDelete: onDeleteHandle,
    onClickRow: onClickRow
  }));
};

module.exports = Main;
//# sourceMappingURL=index.js.map
